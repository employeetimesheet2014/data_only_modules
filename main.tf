terraform {
  required_version = ">= 0.14.9"
  backend "s3" {
    bucket = "teleport-state"
    key    = "terraform-data-only.tfstate"
    region = "ap-southeast-1"
  }
}

data "aws_iam_role" "pg_iam_role_test" {
  name = "pg-${local.env}-iam_role-test"
}
