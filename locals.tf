locals {
  isStagingOrProd = contains(["staging", "production"], terraform.workspace ) ? 1 : 0
  env = terraform.workspace 
}