output "pg_iam_role_test" {
  value = data.aws_iam_role.pg_iam_role_test
  description = "Test IAM role"
}