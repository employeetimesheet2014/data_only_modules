

data "aws_iam_role" "teleport_iam_role_lambda" {
  count = local.isStagingOrProd == true ? 1 : 0
  name = "teleport_${local.env}_iam_role_lambda"
}
