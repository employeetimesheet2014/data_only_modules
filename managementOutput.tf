output "teleport_iam_role_lambda" {
  value =  ["${data.aws_iam_role.teleport_iam_role_lambda[*]}"]
  description = "Labda IAM role"
}
